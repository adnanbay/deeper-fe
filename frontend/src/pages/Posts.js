import React, { useState } from 'react'

export default function Posts() {

    const [posts, setPosts] = useState([
        {
            "userId": 1,
            "id": 1,
            "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
            "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
          },
          {
            "userId": 1,
            "id": 2,
            "title": "qui est esse",
            "body": "est rerum tempore vitae\nsequi sint nihil reprehenderit dolor beatae ea dolores neque\nfugiat blanditiis voluptate porro vel nihil molestiae ut reiciendis\nqui aperiam non debitis possimus qui neque nisi nulla"
          },
          {
            "userId": 1,
            "id": 3,
            "title": "ea molestias quasi exercitationem repellat qui ipsa sit aut",
            "body": "et iusto sed quo iure\nvoluptatem occaecati omnis eligendi aut ad\nvoluptatem doloribus vel accusantium quis pariatur\nmolestiae porro eius odio et labore et velit aut"
          },
          {
            "userId": 1,
            "id": 4,
            "title": "eum et est occaecati",
            "body": "ullam et saepe reiciendis voluptatem adipisci\nsit amet autem assumenda provident rerum culpa\nquis hic commodi nesciunt rem tenetur doloremque ipsam iure\nquis sunt voluptatem rerum illo velit"
          },
          {
            "userId": 1,
            "id": 5,
            "title": "nesciunt quas odio",
            "body": "repudiandae veniam quaerat sunt sed\nalias aut fugiat sit autem sed est\nvoluptatem omnis possimus esse voluptatibus quis\nest aut tenetur dolor neque"
          }
    ])
  return (
    <div style={{display:"flex", padding:"20px", flexWrap: "wrap"}}>
        
        {posts.map((post) => {
            return (
            <div style={{padding:"20px", border: "1px solid lightblue", width:"150px"}}>
                <div>
                <img width={150} src='https://akcdn.detik.net.id/community/media/visual/2023/07/07/pss-sleman-vs-persis-solo-berakhir-imbang-2-2-di-stadion-maguwoharjo-jumat-772023_169.jpeg?w=700&q=90'></img>
            </div>
            <div style={{fontWeight:600}}>{post.title}</div>
            <div>{post.id}</div>
            </div>
            );
        })}
        
        
        
        {/* <div style={{padding:"20px", border: "1px solid lightblue"}}>
            <div>
                <img width={200} src='https://akcdn.detik.net.id/community/media/visual/2023/07/07/pss-sleman-vs-persis-solo-berakhir-imbang-2-2-di-stadion-maguwoharjo-jumat-772023_169.jpeg?w=700&q=90'></img>
            </div>
            <div style={{fontWeight:600}}>{post[0].title}</div>
            <div>Ini Penulis</div>
        </div> */}
    </div>
  )
}
