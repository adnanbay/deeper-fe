import React, { useState } from 'react';
import '../App.css';
import { useNavigate } from 'react-router-dom';


export default function Login() {
  const [username, setUsername] = useState(null)
  const navigate = useNavigate();

  const handleLogin = () => {
    // call API cek validasi LOGIN 
    // simpan akses token
    
    const accessToken = username;

    //simpan akses token ke local storage

    localStorage.setItem('accessToken', accessToken);
    navigate("/dashboard")
  }
  return (
    <div style={{marginTop:"50px", backgroundColor:"lightblue"}}>
        <center>
          <input 
          placeholder="Username"
          onChange={(e) => {
            setUsername(e.target.value);
          }}
          />
          <button onClick={handleLogin}>Submit</button>
        </center>
    </div>
  )
}
