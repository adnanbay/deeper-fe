import React, { useState } from 'react'

export default function SinglePost() {
    
    const [post, setPost] = useState({
        "userId": 1,
    "id": 1,
    "writer": "Megan Fox",
    "firstName": "Megan",
    "lastName": "Fox",
    "title": "sunt aut facere repellat provident occaecati excepturi optio reprehenderit",
    "body": "quia et suscipit\nsuscipit recusandae consequuntur expedita et cum\nreprehenderit molestiae ut ut quas totam\nnostrum rerum est autem sunt rem eveniet architecto"
    })
  
    return (
    
    
    
    <div>
        <table style={{border: " 1px solid black"}}>
            <tr>
                <td>Penulis</td>
                <td>:</td>
                <td>{post.firstName} {post.lastName}</td>
            </tr>
            <tr>
                <td>Judul</td>
                <td>:</td>
                <td>{post.title}</td>
            </tr>
            <tr>
                <td>Isi</td>
                <td>:</td>
                <td>{post.body}</td>
            </tr>
        </table>
    </div>
  )
}
