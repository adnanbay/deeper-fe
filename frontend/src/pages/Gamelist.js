import React, { useState } from 'react'
import { Link } from 'react-router-dom';

export default function Gamelist() {


    const [posts, setPosts] = useState([
        {
            "id":1,
            "title": "Game One",
            "player1": "Ada",
            "player1_choice": "Paper",
            "player2": "Idi",
            "player2_choice": "Rock",
        },
        {
            "id":2,
            "title": "Game Two",
            "player1": "Ado",
            "player1_choice": "Paper",
            "player2": "Idi",
            "player2_choice": "Rock",
        },
        {
            "id":3,
            "title": "Game Three",
            "player1": "LPapa",
            "player1_choice": "Paper",
            "player2": "Idi",
            "player2_choice": "Rock",
        },
        {
            "id":4,
            "title": "Game Four",
            "player1": "Lpipi",
            "player1_choice": "Paper",
            "player2": "Idi",
            "player2_choice": "Rock",
        },
        {
            "id":5,
            "title": "Game Five",
            "player1": "Mandsa",
            "player1_choice": "Paper",
            "player2": "Idi",
            "player2_choice": "Rock",
        },
          
    ])


  return (
    
    
    <div style={{display:"flex", padding:"20px", flexWrap: "wrap"}}>
        
        {posts.map((post) => {
            return (
            <Link to={`${post.id}`} style={{textDecoration:"none"}} key={post.id}>
                <div style={{padding:"20px", border: "1px solid lightblue", width:"300px"}}>
                    <div style={{fontWeight:600}}>{post.title}</div>
                    <div> Player 1 {post.player1} : {post.player1_choice}</div>
                    <div> Player 2 {post.player2} : {post.player2_choice}</div>
                </div>
            </Link>
            );
        })}
        
    </div>
    
  )
}
