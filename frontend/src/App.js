import { Navigate, Route, Routes } from 'react-router-dom';
import './App.css';
import Login from './pages/Login';
import Dashboard from './pages/Dashboard';
import Registration from './pages/Registration';
import Layout from './components/Layout';
import SinglePost from './pages/SinglePost';
import Posts from './pages/Posts';
import Gamelist from './pages/Gamelist';
import Game from './pages/Game';

// function App() {
//   return (
//     <div>Ini div</div>
//   );
// }

const App = () => {
  
  const ProtectedRoute = ({children}) => {
    const accessToken = localStorage.getItem("accessToken");
    if(!accessToken){
      return <Navigate to={"/login"}/>;
    }else {
      return children;
    }
  }


  return <Routes>
    <Route path = "/singlepost" element = {<SinglePost />} />
    <Route path = "/posts" element = {<Posts />} />
    <Route path = "/gamelist" element = {<Gamelist />} />
    <Route path = "/gamelist/:id" element = {<Game />} />
    <Route path = "/" element = {<Layout />}>
      <Route path = "" element = {<div>Path Kosong</div>}/>
      <Route path = "/dashboard" element = {<ProtectedRoute> <Dashboard /> </ProtectedRoute>}/>
      <Route path = "/login" element = {<Login />}/>
      <Route path = "/registration" element = {<Registration />}/>
    </Route>
  </Routes>
}

export default App;
