import React from 'react'
import { Link, Outlet } from 'react-router-dom'

export default function Layout() {
  return (
    <div>
        <div style={({display:"flex", marginLeft:"50px"})}>
            <Link to={"/Dashboard"}><div style={({marginLeft:"20px", backgroundColor: "lightcoral"})}>Dashboard</div></Link>
            <Link to={"/Registration"}><div style={({marginLeft:"20px", backgroundColor: "lightsalmon"})}>Registration</div></Link>
            <Link to={"/Login"}><div style={({marginLeft:"20px", backgroundColor: "lightblue"})}>Login</div></Link>
        </div>
        <div> <Outlet /></div>
    </div>
  )
}
