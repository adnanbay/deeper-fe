const Button = (props) => {
    
    let buttonTitle = "Klik Me"

    if (props.title){
        buttonTitle = props.title;
    }
    
    
    return <button 
    style={{
        backgroundColor: "yellow", 
        padding:"10px", 
        borderRadius:"10px"}}
    >
    {buttonTitle}
    </button>
}

export default Button;